#include <ESP8266WiFi.h>
#include <WiFiClient.h>

// Hardcode WiFi parameters as this isn't going to be moving around.
const char* ssid = "ACCESS_POINT_POLYTECH";
const char* password = "password";

// Start a TCP Server on port 5045
WiFiServer server(5045);

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid,password);
  Serial.println("");
  //Wait for connection
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("Connected to "); Serial.println(ssid);
  Serial.print("IP Address: "); Serial.println(WiFi.localIP());
 
  // Start the TCP server
  server.begin();
}

WiFiClient client;
void loop() {
  // listen for incoming clients
  client = server.available();
  if (client){
    Serial.println("Client connected");
    while (client.connected()){
        while (client.available()>0) {
          String req = client.readStringUntil('\n');
          Serial.println(req);  
          client.flush();
        }

        delay(10);
        /*
        // Read the incoming TCP command
        int actuallyRead = client.read(buffer, BUF_SIZE);
        char cmd[BUF_SIZE];
        //conversion int vers char
        sprintf(cmd, "%s" , buffer);
        Serial.println(cmd);
        */
    }
    Serial.println("Client deconnecté");
    client.stop();
  }
}
