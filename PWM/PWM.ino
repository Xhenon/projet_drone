#define pwm1 13 //PWM sur 8 bits
#define pwm2 8  //numero des ports à verifier
#define pwm3 9
#define pwm4 12

void setup() {
  pinMode(pwm1, OUTPUT);
  pinMode(pwm2, OUTPUT);
  pinMode(pwm3, OUTPUT);
  pinMode(pwm4, OUTPUT);
}

void loop() {
  analogWrite(pwm1,256);
}
