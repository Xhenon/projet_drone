const int echo1=11;
const int trig=13;

void setup() {
  //Serial.begin(9600);
  //pinMode(echo1, INPUT);
  //pinMode(trig, OUTPUT);
  pinMode(1, OUTPUT);
  digitalWrite(1, LOW);
  //pinMode(15, OUTPUT);
  //digitalWrite(15, HIGH);
}

void loop() {
  /*
  long dur;
  long dis;
  long tocm;
  digitalWrite(trig,LOW);
  delayMicroseconds(2);
  digitalWrite(trig,HIGH);
  delayMicroseconds(10);
  digitalWrite(trig,LOW);
  dur=pulseIn(echo1,HIGH, 3000000);
  tocm=microsecondsToCentimeters(dur);
  Serial.println(String(tocm));
  delay(100);*/
}

long microsecondsToCentimeters(long microseconds){
  /* V = vitesse du son dans l'air à 20° = 340 m.s-1 => 1/V donne le facteur 29 */
  /* Facteur 2 car on mesure le temps d'un aller retour */
  return microseconds / 29 / 2; 
}
